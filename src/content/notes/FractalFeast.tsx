import React from 'react';
import {Note} from '../../components/Notes';

export function FractalFeast() {
    return <Note
        title={'Fractal Feast'}
        date={'12-24-20'}
    >
        <p>A few years ago, for no particular occasion, I prepared a fractal feast. My guests had a range of "normal",
            vegan, ketogenic, and kosher diets. If you accommodate all these dietary restrictions, it is probably
            impossible
            to accommodate nut allergies. But if you can deal with some carbs, swap out anything almond flour with a
            more
            traditional recipe; you won't be able to claim Mandelbrot, unfortunately.
        </p>
        <label>The menu:</label>
        <ul>
            <li>Romanesco broccoli</li>
            <li>Sierpinski-Mandelbrot pizza</li>
            <li>Menger sponge cake</li>
        </ul>
        <p>The pictures from the evening came out lousy, so I won't share them here. If you want illustrations, I dunno,
            send me an email or something.</p>
        <p><b>Romanesco broccoli</b> is easy to make. Use your favorite broccoli/cauliflower recipe, I didn't come up
            with
            anything particularly fancy. This is a dairy dish, so maybe you want to put some cheese on it. Parm? I bet
            it'd be good with soy sauce. Caution: season this, or it'll be plain!</p>
        <p><b>Sierpinski-Mandelbrot pizza</b> is a hexagonal pizza with an almond flour crust, with little triangles on
            top. I used Daiya "cheese" shreds for this, but if you're not vegan you're sure to have better results with
            so-called "real cheese".</p>
        <label>Ingredients I used for the crust:</label>
        <ul>
            <li>3/4 cup almond flour</li>
            <li>1 1/2 cups "cheese" shreds</li>
            <li>1 "egg"</li>
            <li>1 tbs seasoning</li>
            <li>1/2 tsp salt</li>
        </ul>
        <br/>
        <label>and the recipe:</label>
        <ol>
            <li>Melt cheese</li>
            <li>Mix in all other ingredients</li>
            <li>Form into a ball, roll out, cut into hexagon. Reserve leftover outer edges for the design</li>
            <li>Broil for 10 minutes, flipping halfway through</li>
            <li>Add sauce, additional cheese, and arrange the leftover dough into Sierpinski triangles</li>
            <li>Broil another minute or so</li>
            <li>Wham, bam, done!!</li>
        </ol>

        <p>Finally, the <b>Menger sponge cake</b>. This is your warning: this fell apart when I made
            it.
            I suggest you instead use your preferred sponge cake recipe and hope for the best. For the masochists
            among us, read on.
        </p>
        <label>You'll need:</label>
        <ul>
            <li>3/4 cup unsweetened apple sauce</li>
            <li>1/4 cup neutral vegetable oil</li>
            <li>3/4 cup white sugar</li>
            <li>1 tbs apple cider vinegar</li>
            <li>1 1/2 cups flour</li>
            <li>1/2 tsp baking soda</li>
            <li>1 tsp baking powder</li>
            <li>a pinch of salt</li>
            <li>vanilla extract</li>
        </ul>

        The recipe:
        <ol>
            <li>Preheat oven to 180˚</li>
            <li>Whisk together applesauce, oil, and sugar in largish mixing bowl until creamy</li>
            <li>Add 1 tbs apple cider vinegar, and stir</li>
            <li>Sift in flour, baking soda, baking powder, and salt, gently mix</li>
            <li>Fold in vanilla (to taste, I suppose)</li>
            <li>Pour into baking dish (can't recall what size....), and bake for 35-40 minutes</li>
            <li>Try to cut into a Menger sponge??</li>
        </ol>
        Let me know if you try these foods and how they turn out!
    </Note>;
}